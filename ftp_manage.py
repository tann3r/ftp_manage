import subprocess
import argparse
import bsddb
import sys
import os
import copy
import socket
from pwgen import pwgen
import slacker
import logging

from settings import slack_token, USER_DB, USER_CFG, USER_CFG_DIR

logger = logging.getLogger('ftp_manage')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
fh = logging.FileHandler('ftp_manage.log')
ch.setLevel(logging.DEBUG)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)


def is_valid_path(parser, arg):
    if not os.path.isdir(arg):
        parser.error('wrong path: %s' % (arg, ))

    return arg

def restart_ftp():
    p = subprocess.Popen(['service', 'vsftpd', 'restart'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()

    if err and err.strip():
        logger.error('Error during restarting service: %s' % (err.strip(), ))
        parser.error('Error during restarting service: %s' % (err.strip(), ))

    return out.strip()

def send_cred_to_slack(slack_username, ftp_username, ftp_pass):
    slack = slacker.Slacker(slack_token)
    try:
        slack.chat.post_message(
            '@%s' % slack_username,
            'ftp@%s: %s // %s' % (socket.gethostname(), ftp_username, ftp_pass),
            as_user='buggy_bot'
            )
        logger.info('Credentials were successfully sent to @%s via Slack' % (slack_username, ))
    except slacker.Error as e:
        logger.warning('Failed to post to slack')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Vsftpd user manager",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog="""
ftp_manage -u new_user -p '/var/www/html/new_user_ftpdir' -c -s slack_username
ftp_manage -u new_user -d
ftp_manage -u new_user -m
        """
        )

    parser.add_argument('-u', '--username', nargs=1, required=True, help='username of modifying user', type=str)
    parser.add_argument('-p', '--ftp-path', nargs=1, required=False, help='path to user\'s ftp home', type=lambda x: is_valid_path(parser, x))
    parser.add_argument('-c', '--create', required=False, help='create user', action='store_true')
    parser.add_argument('-m', '--modify', required=False, help='modify user\'s password', action='store_true')
    parser.add_argument('-d', '--delete', required=False, help='delete user', action='store_true')
    parser.add_argument('-t', '--db-path', nargs=1, required=False, help='path to vsftpd database', type=lambda x: is_valid_path(parser, x))
    parser.add_argument('-s', '--slack-username', nargs=1, required=False, help='slack username where will be the password sent', type=str)

    args = parser.parse_args()

    if sum((args.create, args.delete, args.modify)) >= 2:
        parser.error('choose one of options')

    if args.db_path:
        USER_DB = args.db_path[0]

    if args.create:
        if not args.ftp_path:
            parser.error('please specify ftp path for new user')
        else:
            username = args.username[0]
            db = bsddb.hashopen(USER_DB, 'c')
            if username in db:
                parser.error('user already exists, to change user\'s password use -m option')
            else:
                passw = pwgen(15, symbols=False)
                db[username] = passw
                logger.info("User was created successfully")
                print "%s // %s" % (username, passw)

                user_cfg = copy.copy(USER_CFG)
                user_cfg['local_root'] = args.ftp_path[0]
                with open(os.path.join(USER_CFG_DIR, username), 'w') as outfile:
                    outfile.write('\n'.join(("%s=%s" % (k, v) for k, v in user_cfg.iteritems())))
                    outfile.write('\n')

                logger.info('Restarting vsftpd...')
                service_out = restart_ftp()
                logger.info(service_out)

                # not a pythonic way though
                logger.info('Setting group owner "web" for ftp dir recursively...')
                p = subprocess.Popen(['chgrp', '-R','web', args.ftp_path[0]])
                std, err = p.communicate()
                if err and err.strip():
                    logger.warning("Failed to change ftp dir group owner: %s" % (err.strip(), ))

                logger.info('Setting 775 rights for ftp dir recursively...')
                p = subprocess.Popen(['chmod', '-R', '775', args.ftp_path[0]])
                std, err = p.communicate()
                if err and err.strip():
                    logger.warning("Failed to change rights for ftp dir: %s" % (err.strip(), ))

                logger.info('Setting default group rwx permissions on %s...' % (args.ftp_path[0],))
                p = subprocess.Popen(
                    ['setfacl', '-R', '-d', '-m', 'g::rwx', args.ftp_path[0]],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE
                    )
                std, err = p.communicate()
                if err and err.strip():
                    logger.warning("Failed to set permissions: %s" % (err.strip(), ))

                if args.slack_username:
                    send_cred_to_slack(args.slack_username[0], username, passw)

            db.sync()
            db.close()

    if args.modify:
        username = args.username[0]
        db = bsddb.hashopen(USER_DB, 'c')
        if username in db:
            passw = pwgen(15, symbols=False)
            db[username] = passw
            logger.info("Password for %s has changed successfully" % (username, ))
            print "%s // %s" % (username, passw)
            db.sync()
            db.close()

            logger.info('Restarting vsftpd...')
            service_out = restart_ftp()
            logger.info(service_out)

            if args.slack_username:
                send_cred_to_slack(args.slack_username[0], username, passw)
        else:
            db.sync()
            db.close()
            parser.error('user doesn\'t exist')

    if args.delete:
        username = args.username[0]
        db = bsddb.hashopen(USER_DB, 'c')
        if username in db:
            del db[username]
            logger.info("%s was deleted successfully" % (username,))

            os.remove(os.path.join(USER_CFG_DIR, username))

            db.sync()
            db.close()

            logger.info('Restarting vsftpd...')
            service_out = restart_ftp()
            logger.info(service_out)
        else:
            db.sync()
            db.close()
            parser.error('user doesn\'t exist')


    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
