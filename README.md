ftp_manage
==============

Usage:
```
ftp_manage.py [-h] -u USERNAME [-p FTP_PATH] [-c] [-m] [-d]
                     [-t DB_PATH] [-s SLACK_USERNAME]
```

About:
```
Small vsftpd account manager for company needs
```

Options:
```

  -u USERNAME, --username USERNAME
                        username of modifying user
  -p FTP_PATH, --ftp-path FTP_PATH
                        path to user's ftp home
  -c, --create          create user
  -m, --modify          modify user's password
  -d, --delete          delete user
  -t DB_PATH, --db-path DB_PATH
                        path to vsftpd database
  -s SLACK_USERNAME, --slack-username SLACK_USERNAME
                        slack username where will be the password sent

```

Examples of use:
```
ftp_manage -u new_user -p '/var/www/html/new_user_ftpdir' -c -s slack_username
ftp_manage -u new_user -d
ftp_manage -u new_user -m
```